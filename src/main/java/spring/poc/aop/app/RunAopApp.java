package spring.poc.aop.app;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import spring.poc.aop.config.BeansConfig;
import spring.poc.aop.services.BankService;

public class RunAopApp {

	public static void main(String[] args) {

		//ApplicationContext ctx = new ClassPathXmlApplicationContext("beans.xml");
		ApplicationContext ctx = new AnnotationConfigApplicationContext(BeansConfig.class);
		//BankService bankService = ctx.getBean("bankService",BankService.class);
		BankService bankService = ctx.getBean(BankService.class);

		bankService.createAccount("Gaurav Priyadarshi");
		bankService.miniStatement("1001");
		bankService.deposit("1001", 10000L);
		bankService.miniStatement("1001");
		bankService.withdraw("1001", 10000L);
		bankService.miniStatement("1001");

		((AnnotationConfigApplicationContext)ctx).close();

	}

}