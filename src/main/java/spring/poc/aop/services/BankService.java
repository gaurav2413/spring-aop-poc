package spring.poc.aop.services;

import java.util.HashMap;
import java.util.Map;
import org.springframework.stereotype.Service;

@Service
public class BankService {

	private static Long accountIndex = 1000L;
	private Map<String,Map<String,Object>> accounts;

	public BankService() {
		accounts = new HashMap<String, Map<String,Object>>();
	}

	public void createAccount(String name) {
		Map<String,Object> account = new HashMap<String, Object>();
		account.put("name", name);
		String accountNumber = ++accountIndex+"";
		account.put("accountNumber", accountNumber);
		account.put("ammount", 0);
		accounts.put(accountNumber, account);
		System.out.println("Account has been created successfully");
		System.out.println("Details : "+account);
	}

	private Map<String,Object> validateAccount(String accountNumber) {
		//System.out.println("Validating account......");
		if(accountNumber == null || accountNumber.isEmpty()) {
			System.out.println("Invalid account number!");
			return null; 
		}
		Map<String,Object> account = accounts.get(accountNumber);
		if(account == null){
			System.out.println("Invalid account number!Make sure you are providing correct account details");
			return null;
		}
		return account;
	}

	public boolean deposit(String accountNumber, Long ammount) {
		Map<String,Object> account = validateAccount(accountNumber);
		if(account == null)
			return false;

		Long availableBalance = Long.valueOf(account.get("ammount")+"");
		availableBalance+=ammount;
		account.put("ammount", availableBalance);
		System.out.println("Your account has been successfully deposited with ammount of : "+ammount);
		System.out.println("Your current balance is : "+availableBalance);
		return true;
	}

	public boolean withdraw(String accountNumber, Long ammount) {
		Map<String,Object> account = validateAccount(accountNumber);
		if(account == null)
			return false;
		Long availableBalance = (Long)account.get("ammount");
		if(ammount > availableBalance){
			System.out.println("You don't have enough balance to withdraw!Please enter correct ammount");
			return false;
		}
		availableBalance-=ammount;
		account.put("ammount", availableBalance);
		System.out.println("Your current balance is : "+availableBalance);
		return true;

	}

	public void miniStatement(String accountNumber) {
		Map<String,Object> account = validateAccount(accountNumber);
		if(account != null){
			System.out.println("----------Mini Statement----------");
			System.out.println(account);
		}
	}

}