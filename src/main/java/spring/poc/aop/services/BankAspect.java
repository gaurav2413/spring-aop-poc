package spring.poc.aop.services;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class BankAspect {

	/**
	 * execution(public boolean withdraw())
	 * this advice run whenever its found above method signature
	 */
	/*@Before("execution(public boolean withdraw())")
	public void sendWithdrawMessage1(){
		System.out.println("Your account has been debated....");
	}*/
	
	/**
	 * execution(public boolean withdraw(..))
	 * this advice run whenever its found above method signature with zero or more arguments 
	 */
	@Before("execution(public boolean withdraw(..))")
	public void sendWithdrawMessage2(){
		System.out.println("Your account has been debated....");
	}
	
	/**
	 * execution(public * *(..))
	 * this advice run whenever its found above method signature with zero or more arguments,here no matter what is method name or its return type
	 */
	/*@Before("execution(public * *(..))")
	public void showMessage1(){
		System.out.println("invoked method which takes zero or more arguments,where no matter what is method name");
	}*/
	
	/**
	 * execution(public * *(*))
	 * this advice run whenever its found above method signature with one argument,here no matter what is method name or its return type
	 */
	/*@Before("execution(public * *(*))")
	public void showMessage2(){
		System.out.println("invoked method which takes one argument where no matter what is method name");
	}*/
	
	/**
	 * 
	 */
	/*@Before("allMethods()")
	public void showMessage3(){
		System.out.println("Invoked methods which is call before calling any method.....");
	}*/
	
	/**
	 * 
	 */
	/*@Pointcut("execution( * *(..))")
	public void allMethods(){
		
	}
	
	@Before("args(name)")
	public void showMessage4(String name){
		System.out.println("invoke advice which takes name as a argument "+name);
	}*/
	
}