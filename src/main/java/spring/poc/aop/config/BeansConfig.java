package spring.poc.aop.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ComponentScan({"spring.poc.aop.services"})
@Import(AopConfig.class)
public class BeansConfig {

	/*@Bean(name="bankService")
	public BankService getBankService(){
		return new BankService();
	}*/

}